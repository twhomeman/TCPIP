﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Drawing.Drawing2D;
using System.IO;
using Chroma.TCPIP;
using System.Diagnostics;

namespace MyTestCShape
{
    
    public partial class Form_TCPIP_Demo : Form
    {
        public Form_TCPIP_Demo()
        {
            InitializeComponent();
            //委派指定方法
            UI_Delegate = UpdateMeathod;
            List_Delegate = UpdateConnectList;
        }

        //Form1 UI委派
        public delegate void UpdateForm(string text);
        public UpdateForm UI_Delegate;
        public delegate void UpdateUserList(List<TCPIP.Server.ClientNode> info);
        public UpdateUserList List_Delegate;

        //實作委派方法
        public void UpdateMeathod(string param1)
        {
            //string time = DateTime.Now.ToString("yyyy/MM/dd-HH:mm:ss:fff");
            string time = DateTime.Now.ToString("HH:mm:ss:fff");
            TB_Receive.AppendText(time + "=> " + param1 + Environment.NewLine);

            ListViewItem item = new ListViewItem("R"); //ID   
            item.SubItems.Add(time); //Time
            item.SubItems.Add(param1); //Data
            LV_History.Items.Add(item);
            if (LV_History.Items.Count > 200)
                LV_History.Items.RemoveAt(0);
            LV_History.Items[LV_History.Items.Count - 1].EnsureVisible();
        }

        public void UpdateConnectList(List<TCPIP.Server.ClientNode> info)
        {
            LB_ConnectList.Items.Clear();
            for (int i = 0; i < info.Count; i++)
            {
                LB_ConnectList.Items.Add(info[i].ip + ":" + info[i].port);
            }
        }

        TCPIP.Server tcpip_server;
        TCPIP.Client tcpip_client;

        //範例檔案
        class TCPIP_Server : TCPIP.Server
        {
            //若有介面UI控制，加入Form的class
            public Form_TCPIP_Demo mainform;
            //建構子傳入Form
            public TCPIP_Server(Form_TCPIP_Demo target_form)
            { mainform = target_form; }

            //指令解析
            public override string DoWork(string[] data)
            {
                string reply = "";
                /*
                【範例】
                對方傳送至我方server『cmd 1 2』
                解析data為：
                data[0] = cmd
                data[1] = 1
                data[2] = 2
                 */
                Show(string.Join(this.DelimiterChar.ToString(), data));
                switch (data[0])
                {
                    case "Reply":
                        reply = string.Join(this.DelimiterChar.ToString(), data);
                        break;
                    default:
                        break;
                }
                return reply;
            }

            //呼叫介面UI委派(連線清單)
            public override void UpdateList(List<ClientNode> info)
            { mainform.Invoke(mainform.List_Delegate, info); }

            //呼叫介面UI委派(訊息顯示)
            public void Show(string text)
            { mainform.Invoke(mainform.UI_Delegate, text); }
        }

        class TCPIP_Client : TCPIP.Client
        {
            //若有介面UI控制，加入Form的class
            public Form_TCPIP_Demo mainform;
            //建構子傳入Form
            public TCPIP_Client(Form_TCPIP_Demo target_form)
            { mainform = target_form; }

            //指令解析
            public override string DoWork(string[] data)
            {
                Show(string.Join(this.DelimiterChar.ToString(), data));
                switch (data[0])
                {
                    case "Test":
                        MessageBox.Show("Test cmd");
                        break;
                }
                return "";
            }
            //呼叫介面UI委派(訊息顯示)
            public void Show(string text)
            { mainform.Invoke(mainform.UI_Delegate, text); }
        }

        //----------------------------------------按鍵事件----------------------------------------
        Color Color_Button_Enable = Color.ForestGreen;
        Color Color_Button_Disable = Color.WhiteSmoke;
        private void Button_Connect_Click(object sender, EventArgs e)
        {
            
            IPAddress ipAddress;
            if (IPAddress.TryParse(TB_IP.Text, out ipAddress) != true)
            {
                MessageBox.Show("IP格式錯誤，請重新輸入", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int t_port = Convert.ToInt32(TB_Port.Text);
            bool IsConnect = false;
            if (RB_Server.Checked == true)
            {
                tcpip_server = new TCPIP_Server(this)
                { Port = t_port, DelimiterChar = ',' };
                tcpip_server.BroadCast = Properties.Settings.Default.broadcast;
                IsConnect = tcpip_server.Connect();
                Properties.Settings.Default.type = 0; 
            }
            if (RB_Client.Checked == true)
            {
                tcpip_client = new TCPIP_Client(this)
                { Ip = TB_IP.Text, Port = t_port, DelimiterChar = '\0', Retry = true};
                IsConnect = tcpip_client.Connect();
                Properties.Settings.Default.type = 1;
            }

            if (IsConnect)
            {
                Button_Connect.Enabled = false;
                Button_Disconnect.Enabled = true;
                Button_Connect.ForeColor = Color_Button_Disable;
                Button_Disconnect.ForeColor = Color_Button_Enable;
            }

            Properties.Settings.Default.ip = TB_IP.Text;
            Properties.Settings.Default.port = TB_Port.Text;
            Properties.Settings.Default.Save();
        }

        private void Button_Disconnect_Click(object sender, EventArgs e)
        {
            if (RB_Server.Checked == true)
            {
                tcpip_server.Disconnect();
                LB_ConnectList.Items.Clear();
            }
            if (RB_Client.Checked == true)
            {
                tcpip_client.Disconnect();
            }
            Button_Connect.Enabled = true;
            Button_Disconnect.Enabled = false;
            Button_Disconnect.ForeColor = Color_Button_Disable;
            Button_Connect.ForeColor = Color_Button_Enable;
        }

        public void Button_Send_Click(object sender, EventArgs e)
        {
            string data = TB_SendText.Text;
            string time = DateTime.Now.ToString("HH:mm:ss:fff");
            if (RB_Server.Checked == true)
            {
                if (tcpip_server == null)
                    return;
                tcpip_server.Send(LB_ConnectList.SelectedIndex, data);
            }
            if (RB_Client.Checked == true)
            {
                if (tcpip_client == null)
                    return;
                tcpip_client.Send(data);
            }
            ListViewItem item = new ListViewItem("S"); //ID   
            item.SubItems.Add(time); //Time
            item.SubItems.Add(data); //Data
            LV_History.Items.Add(item);
            if (LV_History.Items.Count > 200)
                LV_History.Items.RemoveAt(0);
            LV_History.Items[LV_History.Items.Count - 1].EnsureVisible();
            TB_SendText.Text = "";
        }

        //----------------------------------------視窗效果----------------------------------------
        public Point downPoint = Point.Empty;
        private void Form_TCPIP_Demo_MouseMove(object sender, MouseEventArgs e)
        {
            if (downPoint == Point.Empty)
            {
                return;
            }
            Point location = new Point(
                this.Left + e.X - downPoint.X,
                this.Top + e.Y - downPoint.Y);
            this.Location = location;
        }

        private void Form_TCPIP_Demo_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            downPoint = Point.Empty;
        }

        private void Form_TCPIP_Demo_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            downPoint = new Point(e.X, e.Y);
        }

        public void SetWindowRegion(object sender, EventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath FormPath;
            FormPath = new System.Drawing.Drawing2D.GraphicsPath();
            Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
            FormPath = GetRoundedRectPath(rect, 20);
            this.Region = new Region(FormPath);
        }

        private GraphicsPath GetRoundedRectPath(Rectangle rect, int radius)
        {
            int diameter = radius;
            Rectangle arcRect = new Rectangle(rect.Location, new Size(diameter, diameter));
            GraphicsPath path = new GraphicsPath();
            // 左上角
            path.AddArc(arcRect, 180, 90);
            // 右上角
            arcRect.X = rect.Right - diameter;
            path.AddArc(arcRect, 270, 90);
            // 右下角
            arcRect.Y = rect.Bottom - diameter;
            path.AddArc(arcRect, 0, 90);
            // 左下角
            arcRect.X = rect.Left;
            path.AddArc(arcRect, 90, 90);
            //閉合曲線
            path.CloseFigure();
            return path;
        }

        private void LB_Close_Click(object sender, EventArgs e)
        {
       
        }

        private void LB_Minimized_Click(object sender, EventArgs e)
        {
    
        }

        private void Form_TCPIP_Demo_Load(object sender, EventArgs e)
        {

            this.skinEngine = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));

            this.skinEngine.SkinFile = "Longhorn.ssk";
            TB_IP.Text = Properties.Settings.Default.ip;
            TB_Port.Text = Properties.Settings.Default.port;
            if (Properties.Settings.Default.type == 0)
                RB_Server.Checked = true;
            else
                RB_Client.Checked = true;
        }

        private void RB_Client_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void Form_TCPIP_Demo_KeyDown(object sender, KeyEventArgs e)
        {

        }

        public void SendCmd(string _text)
        {
            TB_SendText.Text = _text;
        }

        private void TB_SendText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Control == true)
            {
                if (e.KeyCode == Keys.S)
                {
                    CmdForm cf = new CmdForm(this);
                    string ReadLine;


                    StreamReader str = new StreamReader(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\cmd.txt", System.Text.Encoding.Default);
                    int count = 1;
                    CmdNode _data = new CmdNode();
                    while ((ReadLine = str.ReadLine()) != null)
                    {
                        if (count % 2 == 1)
                            _data.Title = ReadLine;
                        else
                        {
                            _data.cmd = ReadLine;
                            cf.CmdList.Add(_data);
                            
                        }
                        count++;
                    }
                    str.Close();

                    cf.Show();
                    cf.Location = new System.Drawing.Point(0, 0);
                }
            }
        }
    }
}
