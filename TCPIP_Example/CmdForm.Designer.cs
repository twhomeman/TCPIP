﻿namespace MyTestCShape
{
    partial class CmdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.skinEngine = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));
            this.listBox_Cmd = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button_Auto = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // skinEngine
            // 
            this.skinEngine.SerialNumber = "";
            this.skinEngine.SkinFile = null;
            // 
            // listBox_Cmd
            // 
            this.listBox_Cmd.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_Cmd.FormattingEnabled = true;
            this.listBox_Cmd.ItemHeight = 24;
            this.listBox_Cmd.Location = new System.Drawing.Point(12, 12);
            this.listBox_Cmd.Name = "listBox_Cmd";
            this.listBox_Cmd.Size = new System.Drawing.Size(386, 340);
            this.listBox_Cmd.TabIndex = 0;
            this.listBox_Cmd.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox_Cmd_MouseClick);
            this.listBox_Cmd.SelectedIndexChanged += new System.EventHandler(this.listBox_Cmd_SelectedIndexChanged);
            this.listBox_Cmd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_Cmd_MouseDown);
            // 
            // timer1
            // 
            this.timer1.Interval = 1500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button_Auto
            // 
            this.button_Auto.Location = new System.Drawing.Point(12, 358);
            this.button_Auto.Name = "button_Auto";
            this.button_Auto.Size = new System.Drawing.Size(386, 41);
            this.button_Auto.TabIndex = 1;
            this.button_Auto.Text = "按下後自動發送命令";
            this.button_Auto.UseVisualStyleBackColor = true;
            this.button_Auto.Click += new System.EventHandler(this.button_Auto_Click);
            // 
            // CmdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 411);
            this.Controls.Add(this.button_Auto);
            this.Controls.Add(this.listBox_Cmd);
            this.Name = "CmdForm";
            this.Text = "命令快捷列";
            this.Load += new System.EventHandler(this.CmdForm_Load);
            this.Shown += new System.EventHandler(this.CmdForm_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunisoft.IrisSkin.SkinEngine skinEngine;
        private System.Windows.Forms.ListBox listBox_Cmd;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button_Auto;
    }
}