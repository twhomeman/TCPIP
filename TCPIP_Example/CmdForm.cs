﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyTestCShape
{
    public struct CmdNode
    {
        public string Title;
        public string cmd;
    }

    public enum ValueType
    {
        _int, _double, _string
    }

    public struct ValueNode
    {
        public string Name;
        public ValueType Type;
        public int value_int;
        public string value_double;
        public string value_string;
    }

    public partial class CmdForm : Form
    {
        Dictionary<string, ValueNode> ValueList;
        public List<CmdNode> CmdList;
        public List<LoopNode> LoopList;
        Form_TCPIP_Demo father;
        public CmdForm(Form_TCPIP_Demo ptr)
        {
            InitializeComponent();
            CmdList = new List<CmdNode>();
            LoopList = new List<LoopNode>();
            ValueList = new Dictionary<string, ValueNode>();
            father = ptr;
        }

        private void CmdForm_Load(object sender, EventArgs e)
        {
            skinEngine = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));

            skinEngine.SkinFile = "Longhorn.ssk";
        }

        private void CmdForm_Shown(object sender, EventArgs e)
        {
            for(int i = 0; i < CmdList.Count;i++)
            {
                listBox_Cmd.Items.Add(CmdList[i].Title);
            }
        }

        private void listBox_Cmd_SelectedIndexChanged(object sender, EventArgs e)
        {
            int _index = listBox_Cmd.SelectedIndex;
            string _text = CmdList[_index].cmd;
            father.SendCmd(_text);
        }

        private void listBox_Cmd_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void listBox_Cmd_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int _index = listBox_Cmd.SelectedIndex;
                if (_index != -1)
                {
                    string _text = CmdList[_index].cmd;

                    if (_text.Contains(":=") && _text.Contains("%"))
                    {
                        string[] words = _text.Split('=');
                        string[] value = _text.Split(':');
                        if (!ValueList.ContainsKey(value[0]))
                            ValueList.Add(value[0], GetValueNode(words[1]));
                        else
                        {
                            ValueList.Remove(value[0]);
                            ValueList.Add(value[0], GetValueNode(words[1]));
                        }
                        return;
                    }
                    foreach(string item in ValueList.Keys)
                    {
                        if (_text.Contains(item))
                        {
                            _text = _text.Replace(item, ValueList[item].value_string);
                        }
                    }


                    father.SendCmd(_text);
                    father.Button_Send_Click(sender, e);
                }
            }
        }

        //A:=%time
        private ValueNode GetValueNode(string _text)
        {
            ValueNode ret = new ValueNode();

            if(_text.Contains("%time"))
            {
                string time = DateTime.Now.ToString("yyyyMMdd-HHmmssfff");
                ret.Type = ValueType._string;
                ret.value_string = time;
            }else if (_text.Contains("%r"))
            {
                Random rnd = new Random();
                ret.Type = ValueType._string;
                ret.value_string = rnd.Next(99999).ToString();
            }else if (_text.Contains("%null"))
            {
                ret.Type = ValueType._string;
                ret.value_string = "";
            }

            return ret;
        }

        
        private void button_Auto_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == false)
            {
                button_Auto.Text = "執行中";
                listBox_Cmd.SelectedIndex = 0;
                timer1.Enabled = true;

            }else
            {
                button_Auto.Text = "按下後自動發送命令";
                timer1.Enabled = false;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int _index = listBox_Cmd.SelectedIndex;
            if (CmdList[_index].cmd.Contains("%loop"))
            {
                LoopNode _loop = new LoopNode();
                _loop.loop_count = 0;
                _loop.loop_index = _index + 1;
                string[] word = CmdList[_index].cmd.Split(',');
                _loop.loop_end = Int32.Parse(word[1]);
                LoopList.Add(_loop);
            }

            if (CmdList[_index].cmd.Contains("%endloop"))
            {
                LoopList[LoopList.Count - 1].loop_count ++;
                if (LoopList[LoopList.Count - 1].loop_count < LoopList[LoopList.Count - 1].loop_end)
                    listBox_Cmd.SelectedIndex = LoopList[LoopList.Count - 1].loop_index;
                else
                {
                    if(LoopList[LoopList.Count - 1].loop_end == -1)
                        listBox_Cmd.SelectedIndex = LoopList[LoopList.Count - 1].loop_index;
                    else
                        LoopList.RemoveAt(LoopList.Count - 1);
                }
            }


            var evt = new MouseEventArgs(MouseButtons.Right, 0, 0, 0, 0);
            listBox_Cmd_MouseDown(sender, evt);
            if (listBox_Cmd.SelectedIndex < listBox_Cmd.Items.Count - 1)
                listBox_Cmd.SelectedIndex += 1;
            else
            {
                listBox_Cmd.SelectedIndex = 0;
                timer1.Enabled = false;
            }
        }
    }

    public class LoopNode
    {
        public int loop_index;
        public int loop_count;
        public int loop_end;
    }
}
